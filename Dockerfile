FROM alpine:3.12.3 as build

LABEL maintainer=robert@aztek.io

ARG HELM_VERSION=v3.4.2
ARG HELMFILE_VERSION=v0.135.0

# hadolint ignore=DL3018
RUN apk add --update --no-cache curl
RUN curl -O "https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz"
RUN tar -zxvf "helm-${HELM_VERSION}-linux-amd64.tar.gz"
RUN curl -sLO "https://github.com/roboll/helmfile/releases/download/${HELMFILE_VERSION}/helmfile_linux_amd64" && \
        chmod +x helmfile_linux_amd64

FROM aztek/awscli:1.18.174 as final

LABEL maintainer=robert@aztek.io

COPY --from=build ["linux-amd64/helm", "/bin/"]
COPY --from=build ["helmfile_linux_amd64", "/bin/helmfile"]

# hadolint ignore=DL3018
RUN apk add --no-cache git && \
        helm plugin install https://github.com/databus23/helm-diff

ENTRYPOINT ["/bin/helm"]
CMD ["--help"]
